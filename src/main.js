const Apify = require('apify');
const ApifyClient = require('apify-client');
const Ajv = require('ajv');
const vm = require('vm');

const {log} = Apify.utils;

const ajv = new Ajv({
    nullable: true
});

const apifyClient = new ApifyClient({
    userId: process.env.APIFY_USER_ID,
    token: process.env.CUSTOM_SOURCE_APIFY_TOKEN || process.env.APIFY_TOKEN
});

const getLatestDefaultDatasetIds = async actorIds => {
    const defaultDatasetIds = await Promise.all(actorIds.map(async actorId => {
        const runs = await apifyClient.acts.listRuns({
            actId: actorId,
            desc: true
        });

        const [latestRun] = runs.items;

        if (latestRun)
            return latestRun.defaultDatasetId;
    }));

    return defaultDatasetIds.filter(datasetId => datasetId);
};

Apify.main(async () => {
    const input = await Apify.getInput();

    const {
        actorIds,
        datasetIds = await getLatestDefaultDatasetIds(actorIds),
        itemsLimit,
        outputFormat,
        manageFields,
        sortedFields,
        postProcessing,
        postProcessFun,
        validateOnly,
        validateSchema,
        outputSchema
    } = input;

    const conditions = {
        manageFields: manageFields && (!validateOnly || !validateSchema),
        postProcessing: postProcessing && (!validateOnly || !validateSchema),

    };

    if (manageFields && !conditions.manageFields)
        log.warning('Option "validateOnly" selected - skipping field management!');

    if (postProcessing && !conditions.postProcessing)
        log.warning('Option "validateOnly" selected - skipping post-processing!');

    // const datasetIds = input.datasetIds || await getLatestDefaultDatasetIds(actorIds);
    console.log({input});
    console.log({datasetIds});

    if (!datasetIds.length) {
        console.log('No valid datasets found');
    }

    const dataClusters = await Promise.all(datasetIds.map(async datasetId => {
        console.log(datasetId, 'Loading data...');

        const options = {
            datasetId,
            clean: true,
            limit: itemsLimit
        };

        if (manageFields)
            options.fields = sortedFields;

        const paginationList = await apifyClient.datasets.getItems(options);

        return {
            datasetId,
            paginationList
        }
    }));

    const outputDataset = await Apify.openDataset();
    const adaptedDataset = {};

    for (const dataCluster of dataClusters) {
        const {paginationList: {items, count, limit, total, offset}, datasetId} = dataCluster;
        const log = (...args) => console.log(datasetId, ...args);

        log(`Processing items ${offset}-${offset + count}/${total}`);

        if (conditions.postProcessing) {
            const process = eval(postProcessFun);

            if (typeof process !== 'function')
                throw Error('Failed to parse post processing function!');

            // const process = new Function("item", "item.priceTiers.forEach(tier => {tier.grandTotal = tier.grandTotal || tier.grandTotalMaybe; delete tier.grandTotalMaybe; return tier})");
            items.forEach(item => {
                // item = vm.runInNewContext("item.priceTiers.forEach(tier => {tier.grandTotal = tier.grandTotal || tier.grandTotalMaybe; delete tier.grandTotalMaybe; return tier})", {item})
                process(item);
                // console.log(item);
            });

        }

        if (!validateSchema) {
            await outputDataset.pushData(items);
        } else {
            const invalidItems = [], errorItems = [];

            const validItems = items.filter(item => {
                const valid = ajv.validate(outputSchema, item);

                if (valid)
                    return valid;

                if (item.url) {
                    log(item.url, JSON.stringify(ajv.errors));

                    invalidItems.push({
                        url: item.url
                    });

                } else {
                    log('URL not found - skip item:', JSON.stringify(item));
                    errorItems.push(item);
                }
            });

            adaptedDataset[datasetId] = {
                validItems,
                invalidItems,
                errorItems
            };

            if (!validateOnly)
                await outputDataset.pushData(validItems);
        }
    }

    let recordKey;
    if (conditions.manageFields) {
        const outputDatasetMetadata = await outputDataset.getInfo();
        const outputDatasetId = outputDatasetMetadata.id;
        const outputFields = sortedFields.join(',');

        const href = `https://api.apify.com/v2/datasets/${outputDatasetId}/items?clean=1&attachment=1&format=${outputFormat || 'json'}&fields=${outputFields}`;
        const html = `<html><head><script type="application/javascript">window.location.href = '${href}'</script></head><a href="${href}">${href}</a></html>`;
        recordKey = `DATASET_DOWNLOAD-${sortedFields && 'CUSTOM_FIELD_ORDER-' || ''}${outputFormat.toUpperCase()}`;

        await Apify.setValue(recordKey, html, {contentType: 'text/html'});
    }

    if (validateSchema) {
        await Promise.all(Object.keys(adaptedDataset).map(async datasetId => {
            if (adaptedDataset[datasetId].invalidItems.length)
                Apify.setValue(`${datasetId}-invalidSchemaItems-requestListSources`, adaptedDataset[datasetId].invalidItems)
        }));

        await Promise.all(Object.keys(adaptedDataset).map(async datasetId => {
            if (adaptedDataset[datasetId].errorItems.length)
                Apify.setValue(`${datasetId}-ERROR_ITEMS`, adaptedDataset[datasetId].errorItems)
        }));

        const itemCountsPerDataset = Object.keys(adaptedDataset).map(datasetId => ({
            validItems: adaptedDataset[datasetId].validItems.length,
            invalidItems: adaptedDataset[datasetId].invalidItems.length,
            errorItems: adaptedDataset[datasetId].errorItems.length
        }));

        const itemCountsByCategory = itemCountsPerDataset.reduce((pool, next) => {
            pool.validItems = pool.validItems + next.validItems;
            pool.invalidItems = pool.invalidItems + next.invalidItems;
            pool.errorItems = pool.errorItems + next.errorItems;

            return pool;
        });

        Object.keys(adaptedDataset).map(datasetId => {
            console.log('---');
            console.log(datasetId, 'Valid schema items:', adaptedDataset[datasetId].validItems.length);
            console.log(datasetId, 'Invalid schema items:', adaptedDataset[datasetId].invalidItems.length);
            console.log(datasetId, 'Failed to parse items = ERRORS:', adaptedDataset[datasetId].errorItems.length);
            console.log('---');
        });

        console.log('TOTAL');
        console.log('Valid schema items:', itemCountsByCategory.validItems);
        console.log('Invalid schema items:', itemCountsByCategory.invalidItems);
        console.log('Failed to parse items = ERRORS:', itemCountsByCategory.errorItems);
    }

    console.log('Operation completed - terminate');

    if (conditions.manageFields) {
        const downloadUrl = `https://api.apify.com/v2/key-value-stores/${process.env.APIFY_DEFAULT_KEY_VALUE_STORE_ID}/records/${recordKey}?disableRedirect=true`;
        console.log('---');
        console.log('Download link for dataset with sorted fields:', downloadUrl);
    }
});
